console.log('Hellow Wurld?');
//js
// What are Conditional Statements?

	// Conditional statements allow us to control the flow of our program.

// [SECTION] If, Else If and Else Statement
	
	let numA = -1;

	// [SUB-SECTION] If Statement
		// Executes a statement if a specified condition is true

		if(numA < 0){
			console.log('Hellow');
		};

		/*
			Syntax:

			if(condition){
				statement
			};
		*/

		console.log(numA < 0); 

		numA = 0

		if(numA < 0){
			console.log('Hello again if numA is 0!');
		};

		console.log(numA < 0);

		// Another example:

			let city = 'New York';

			if(city === 'New York'){
				console.log('Welcome to New York City!');
			};

	// [SUB-SECTION] Else If Statement

		/*
			-Executes a statement if previous conditions are false and if the specified condition is true

		*/

			let numH = 1;

			if(numA < 0){
				console.log('Hello!');
			} else if (numH > 0){
				console.log('World');
			};

		// We were able to run the else if() statement after we evaluated that the if condition was failed.

			numA = 1;

			if(numA > 0){
				console.log('Another Hello!');
			} else if (numH > 0){
				console.log('Another World!');
			};

			// Let's another one!

			city = 'Manila';

			if(city === 'New York'){
				console.log('Welcome to New York City!');
			} else if (city === 'Tokyo'){
				console.log('Welcome to Tokyo, Japan!');
			} else if (city === 'Manila'){
				console.log('Welcome to Manila City!')
			};

		// Since we failed the condition for the first if(), we went to the second else if() and checked and instead passed that conditon.

	// [SUB-SECTION] Else Statement

			if(numA < 0){
				console.log('H3llow');
			} else if (numH === 0) {
				console.log('W0rld!');
			} else {
				console.log('Again, again!');
			};

		// Else Statement should only be added if there is a preceding if condition. else statement by itself will not work, however, if statements will work even if there is no else statement.

			/*
			else {
				console.log('Will not run without an if');
			};
			*/

	// [SUB-SECTION] If, Else If, and Else Statements with Functions

		let message = 'No Message.';
		console.log(message);

		function determineTyphoonIntensity(windSpeed){

			if(windSpeed < 30){
				return 'Not a typhoon yet.'
			} else if (windSpeed <= 61){
				return 'Tropical depression detected.';
			} else if (windSpeed >= 62 && windSpeed <= 88){
				return 'Tropical storm detected.';
			} else if (windSpeed >=89 && windSpeed <= 117){
				return 'Severe tropical storm detected.';
			} else {
				return 'Typhoon detected.';
			}
		};

		message = determineTyphoonIntensity(63);
		console.log(message);

		// - We can further control the flow of our program based on conditions and changing variables and results

		if (message == 'Tropical storm detected.'){
			console.warn(message);
		};


// [SECTION] Truthy and Falsy

	// - In Javascript a 'truthy' value is a value that is considered true when encountered in a Boolean context

		/*
			 - Falsy Values/Exception for truthy:
			 	1. false
			 	2. 0
			 	3. -0
			 	4. ""
			 	5. null
			 	6. undefined
			 	7. Nan
		*/

		// [SUB-SECTION] Truthy Examples

			if(true){
				console.log('Truthy');
			};

			if(1){
				console.log('Truthy');
			};

			if([]){
				console.log('Truthy');
			};

		// [SUB-SECTION] Falsy Examples
			if(false){
				console.log('Falsy');
			};

			if(0){
				console.log('Falsy');
			};

			if(undefined){
				console.log('Falsy');
			};

// [SECTION] Conditional (Ternary) Operator

	/*
		- The Conditional (Ternary) Operator takes in three operands:
			1. condition
			2. expression to execute if the condition is truthy
			2. expression to execute if the condition is falsy
		- Syntax
			(expression) ? ifTrue : ifFalse;
	*/	

		// Single Statement Execution
			let ternaryResult = (1 > 18) ? true : false;
			console.log('Result of Ternary Operator: ' + ternaryResult);

		// Multiple Statement Execution
			//let name;

			function isOfLegalAge(){
				name = 'Mike';
				return 'You are of the legal age limit';
			};

			function isUnderAge(){
				name = 'Scottie';
				return 'You are under the age limit';
			};
				/*
					- The 'parseInt' function converts the input received into a number data type.
				*/
			// let age = parseInt(prompt('What is your age'));
			// console.log(typeof age);

			// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
			// console.log('Result of Ternary Opeartor in functions: ' + legalAge + ", " + name)

// [SECTION] Switch Statement

		// - The switch statement evaluates an expression and matches the expression's value to a case clause. The switch will then executes the statements associated with the case, as well as statement in cases that follow the matching case.
		// - The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case condition if the user inputs capitalized or uppercased letters. 

			/*
				Syntax:
					switch (expression) {
						case value:
							statement:
							break:
						default:
							statement:
							break:
					}
			*/

		let day = prompt('What day of the week is it today?').toLowerCase();
		console.log(day);

			switch (day) {
				case 'monday':
					console.log('The color of the day is red!');
					break;
				case 'tuesday':
					console.log('The color of the day is orange!');
					break;
				case 'wednesday':
					console.log('The color of the day is yellow!');
					break;
				case 'thursday':
					console.log('The color of the day is green!');
					break;
				case 'friday':
					console.log('The color of the day is blue!');
					break;
				case 'saturday':
					console.log('The color of the day is indigo!');
					break;
				case 'sunday':
					console.log('The color of the day is violet!');
					break;
				default:
					console.log('Please input a valid day!');
					break;
			};

	// Try-Catch-Finally Statement

		// 'try catch' statements are commonly used for error handling


			function showIntensityAlert(windSpeed){
				try{

					console.log('No Error Here!');
					alerat(determineTyphoonIntensity(windSpeed));
					aler4t(determineTyphoonIntensity(windSpeed));

				} catch (error) {
					console.log(typeof error);

					// 'error.message' is used to access the information to an error object.
					console.warn(error.message);
				} finally {
					
					// Continue execution of code regardless of sucess and failure of code execution in the 'try' block to handle resolve errors.
					alert('Intensity updates will show new alert.')
					console.log('Hellowwww!')
				}
			}

			showIntensityAlert(67)

			function showIntensityAlert1(windSpeed){
				try{

					console.log('No Error Here!');
					alerat(determineTyphoonIntensity(windSpeed));
					alerat(determineTyphoonIntensity(windSpeed));

				} catch (error) {
					console.log(typeof error);

					// 'error.message' is used to access the information to an error object.
					console.warn(error.message);
				} finally {
					
					// Continue execution of code regardless of sucess and failure of code execution in the 'try' block to handle resolve errors.
					alert('Intensity updates will show new alert.')
					console.log('Hellowwww!')
				}
			}

			showIntensityAlert1(67)
